<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ng-class="{'active': activeURL == 'home', '': activeURL != 'home'}">
				<a href="<c:url value="/"/>" title='<spring:message code="header.home"/>'>
					<p> <spring:message code="header.home" /> </p>
				</a>
			</li>
            <li ng-class="{'gray': activeURL == 'chat', '': activeURL != 'chat'}">
				<a href="<c:url value="/protected/chat"/>" title='<spring:message code="header.chat"/>'>
				  	<p>	<spring:message code="header.chats" /> </p>
				</a>
			</li>
			<li ng-class="{'gray': activeURL == 'chat', '': activeURL != 'chat'}">
				<a href="<c:url value="/protected/backoffice"/>" title='<spring:message code="header.chat"/>'>
				  	<p>	<spring:message code="header.backoffice_chat" /> </p>
				</a>
			</li>
		  </ul>
          <ul class="nav navbar-nav navbar-right">
            <li>
            	<a href="<c:url value='/logout' />" title='<spring:message code="header.logout"/>'>
            		<p class="displayInLine">
            			<spring:message code="header.logout" />
						(${user.name})
					</p>
				</a>
			</li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	<div class="chat-logo">
  		<img src="/resources/img/logo.png">
	</div>

