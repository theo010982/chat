<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="chat-logo">
  		<img src="/resources/img/logo.png">
	</div>
<div ng-controller="loginController">
		<form method="post" action="j_spring_security_check"
			class="form-signin formLogin">
			<legend>
				<spring:message code="login.header"/>
			</legend>
			
			<div class="alert-danger"
				ng-class="{'': displayLoginError == true, 'none': displayLoginError == false}">
				<spring:message code="login.error" />
			</div>
			<br>

			<input name="j_username" id="j_username" type="email"
				class="form-control login"
				placeholder="<spring:message code='sample.email' /> "><br />

			<input name="j_password" id="j_password" type="password"
				class="form-control login" placeholder="Password"><br />

			<button type="submit" id="btnLogin" name="submit"
				class="btn btn-lg btn-primary btn-block login">
				<spring:message code="login.signIn" />
			</button>

		</form>

	</div>

<script src="<c:url value='/resources/js/pages/login.js' />"></script>