<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row-fluid">

	<div class="jumbotron ng-scope" ng-controller="BackOfficeChatCtrl">
		<select ng-model="selectedUser"
			ng-options="u.name for u in users | orderBy: 'name'"
			ng-change="findMessageChat()"></select>
		<input
			class="btn-group-xs"
			type="button" ng-click="findMessageChat()" value="Refresh">
		<hr>

		<div id="divBackOffice" class="chat-wrap">
			<ul class="chat">
				<li ng-repeat="chat in backOfficeChat | orderBy:'seguencia':false"" class="chat-message"
					ng-class="{'chat-message-user' : chat.typeMessage=='REQUEST',
								 'chat-message-client' : chat.typeMessage!='REQUEST'}">
					
					<span class="chat-message-icon"> 
						<span class="glyphicon"
							ng-class="{'glyphicon-user' : chat.typeMessage=='REQUEST',
									   'glyphicon-shopping-cart' :  chat.typeMessage!='REQUEST'}">
						</span>
					</span>
					<span class="chat-message-text">	
						<span class="glyphicon"  
							ng-class="{'glyphicon-user' : chat.typeMessage=='MANUAL',
									   'glyphicon glyphicon-flash' :  chat.typeMessage=='AUTO'}" ></span>
						{{chat.mensagemTexto}}
					</span>
				</li>
			</ul>
		</div>
		<input id="ng-dirty" type="text" ng-model="responseMessage"
			ng-disabled="order[order.length - 1].typeMessage != 'AUTO'"
			ng-enter="saveResponseMessage()" class="ng-pristine ng-valid" />
		<input class="btn btn-primary"
			ng-disabled="order[order.length - 1].typeMessage != 'AUTO'"
			ng-class="{'disabled' : responseMessage=='', 'enable'  : newMessage!=''}"
			type="button" ng-click="saveResponseMessage()" value="Enviar">
		<hr>

	</div>
</div>

<script src="<c:url value="/resources/js/pages/chat.js" />"></script>
