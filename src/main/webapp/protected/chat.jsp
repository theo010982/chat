<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row-fluid">
	<div ng-controller="ChatCtrl">
		<div class="jumbotron ng-scope" ng-controller="ChatCtrl">
			<div id="loading" style="display: none;">
				<img id="loading-image" src="/resources/img/ajax-loader.gif"
					alt="Loading..." />
			</div>

			<div class="chat-wrap">
				
				<div class="chat-box-left">
					<div class="chat-header">
						<span>A</span>
					</div>
					
					<ul class="chat">
						<li ng-repeat="chat in chatProfileOne" class="chat-message"
							ng-class="{'chat-message-user' : chat.typeMessage=='REQUEST',
									 'chat-message-client' : chat.typeMessage!='REQUEST'}">
						
							<span class="chat-badge"><span class="badge">Resp</span></span>
							<span class="chat-message-icon"> 
							
								<span class="glyphicon"
									ng-class="{'glyphicon-user' : chat.typeMessage=='REQUEST',
											 'glyphicon-shopping-cart' :  chat.typeMessage!='REQUEST'}">
								</span>
							</span>
							<span class="chat-message-text">{{chat.mensagemTexto}}</span>
						</li>
					</ul>
				</div>
				
				<div class="chat-box-right">
					<div class="chat-header">
						<span>B</span>
					</div>
					<ul class="chat">
						<li ng-repeat="chat in chatProfileTwo" class="chat-message"
							ng-class="{'chat-message-user' : chat.typeMessage=='REQUEST',
									 'chat-message-client' : chat.typeMessage!='REQUEST'}">
						
						<span class="chat-message-icon"> <span class="glyphicon"
							ng-class="{'glyphicon-user' : chat.typeMessage=='REQUEST',
									 'glyphicon-shopping-cart' :  chat.typeMessage!='REQUEST'}">
						</span>
						</span> <span class="chat-message-text">{{chat.mensagemTexto}}</span>
						</li>
					</ul>
				</div>
				
			</div>
			<input id="ng-dirty" type="text" ng-model="newMessage"
				ng-enter="callChat()" class="ng-pristine ng-valid" /> 
			<input
				class="btn btn-primary"
				ng-class="{'disabled' : newMessage=='', 'enable'  : newMessage!=''}"
				type="button" ng-click="callChat()" value="Enviar">
			<hr>
		</div>
		<hr />
	</div>
</div>
<script src="<c:url value="/resources/js/pages/chat.js" />"></script>