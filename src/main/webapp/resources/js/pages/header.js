function LocationController($scope, $location) {
    if($location.$$absUrl.lastIndexOf('/contacts') > 0){
        $scope.activeURL = 'contacts';
    }else if($location.$$absUrl.lastIndexOf('/chat') > 0){
    	$scope.activeURL = 'chat';
	} else{
        $scope.activeURL = 'home';
    }
}