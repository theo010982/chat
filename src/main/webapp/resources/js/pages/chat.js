angular.module('Chat', [])
		
		.service('ChatService', function() {
			
			this.createTwoChats = function($scope) {
				$scope.chatProfileOne = [];
				$scope.chatProfileTwo = [];
				for(var i = 0; i < $scope.chats.length; i += 1){
				    if($scope.chats[i].typeMessage=='REQUEST'){
				    	$scope.chatProfileOne.push($scope.chats[i]);
				    	$scope.chatProfileTwo.push($scope.chats[i]);
				    }else{
				    	switch (Math.floor((Math.random() * 2) + 1)) {
				    		case 1:
				    			$scope.chatProfileOne.push($scope.chats[i]);
						    	$scope.chatProfileTwo.push($scope.chats[++i]);
				    			break;
				    		case 2:
				    			$scope.chatProfileTwo.push($scope.chats[i]);
				    			$scope.chatProfileOne.push($scope.chats[++i]);
				    			break;
				    	}
				    }
				}
			},
 			
			this.orderByCreate = function($scope) {

				$scope.order = $scope.backOfficeChat.sort(function(a, b) {
					return a.seguencia - b.seguencia;
				});

			};
			
		})
		
		.controller(
				'ChatCtrl',

				function ChatCtrl($scope, $http, ChatService) {
					
					renderToTop = function () {
						$(".chat").scrollTop($(".chat")[0].scrollHeight);
					},
					
					$scope.callChat = function() {
						var url = '/protected/chat/';
						$http(
								{
									method : 'POST',
									url : url,
									contentType : "charset=utf-8",
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
										'newMessage' : $scope.newMessage,
										'token' : $scope.token

									}
								}).success(
								function(data, status, headers, config) {
									$scope.chats = data.message;
									$scope.token = data.token;
									$scope.newMessage = '';
									ChatService.createTwoChats($scope);
								}).error(
								function(data, status, headers, config) {
									console.log("Error");
								});
					};
					
				}
				
		)
		.controller(
				'BackOfficeChatCtrl',

				function BackOfficeChatCtrl($scope, $http, ChatService) {
					
					renderToTop = function () {
						$(".chat").scrollTop($(".chat")[0].scrollHeight);
					},
					
					$scope.findUsers = function() {
						var url = '/protected/backoffice/findUsers';
						$http(
								{
									method : 'GET',
									url : url,
									contentType : "charset=utf-8",
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								}).success(
								function(data, status, headers, config) {
									$scope.users = data;
								}).error(
								function(data, status, headers, config) {
									console.log("Error");
								});
					};
					
					$scope.findMessageChat = function() {
						var url = '/protected/backoffice/findMessagebyUser';
						$http(
								{
									method : 'GET',
									url : url,
									contentType : "charset=utf-8",
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
										'idUser' :  $scope.selectedUser.id
									}
								}).success(
								function(data, status, headers, config) {
									$scope.backOfficeChat = data.message;
									$scope.token = data.token;
									$scope.responseMessage = '';
									ChatService.orderByCreate($scope);
									renderToTop();
								}).error(
								function(data, status, headers, config) {
									console.log("Error");
								});
					};
					
					$scope.findUsers();
					
					$scope.saveResponseMessage = function() {
						var url = '/protected/backoffice/saveResponseMessage';
						$http(
								{
									method : 'POST',
									url : url,
									contentType : "charset=utf-8",
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
										'idUser' :  $scope.selectedUser.id,
										'responseMessage' : $scope.responseMessage,
										'token' : $scope.token,
										'lastMessageId' : getLastMessageFromUser().id
									}
								}).success(
								function(data, status, headers, config) {
									$scope.backOfficeChat = data.message;
									$scope.token = data.token;
									$scope.newMessage = '';
									ChatService.orderByCreate($scope);
								}).error(
								function(data, status, headers, config) {
									console.log("Error");
								});
					};
					
					getLastMessageFromUser = function() {
						var result = [] ;
						for(var i = 0; i < $scope.backOfficeChat.length; i += 1){
						    if($scope.backOfficeChat[i].typeMessage=='REQUEST'){
						    	result.push($scope.backOfficeChat[i]);
						    }
						}
						return result[0];
					};
					
				}
						
		).directive('ngEnter', function() {
			return function(scope, element, attrs) {
				element.bind("keydown keypress", function(event) {
					if (event.which === 13) {
						scope.$apply(function() {
							scope.$eval(attrs.ngEnter, {
								'event' : event
							});
						});
						event.preventDefault();
					}
				});
			};
		}).config(function($httpProvider) {
			$httpProvider.responseInterceptors.push('myHttpInterceptor');
			var spinnerFunction = function(data, headersGetter) {
				$('#loading').show();
				return data;
			};
			$httpProvider.defaults.transformRequest.push(spinnerFunction);
		}).factory('myHttpInterceptor', function($q, $window) {
			return function(promise) {
				return promise.then(function(response) {
					$('#loading').hide();
					return response;
				}, function(response) {
					$('#loading').hide();
					return $q.reject(response);
				});
			};
		});