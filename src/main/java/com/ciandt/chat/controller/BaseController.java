package com.ciandt.chat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ciandt.chat.model.User;
import com.ciandt.chat.service.UserService;

public class BaseController {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MessageSource messageSource;
    
    protected User getUserLogged() {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	return userService.findByEmail(auth.getName());
    }
}
