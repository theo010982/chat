package com.ciandt.chat.controller;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ciandt.chat.helper.MessageHelper;
import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;
import com.ciandt.chat.service.AddressService;
import com.ciandt.chat.service.MessageService;
import com.ciandt.chat.service.RobotService;
import com.ciandt.chat.vo.ChatVO;

@Controller
@RequestMapping(value = "/protected/chat")
public class ChatController extends BaseController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private RobotService robotService;

    @Autowired
    private AddressService addressService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
	return new ModelAndView("chat");
    }
    
    @SuppressWarnings("rawtypes")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> callChat(@RequestHeader(required = true, value = "newMessage") String newMessage,
	    @RequestHeader(required = false, value = "token") String token, Locale locale) {

	User user = getUserLogged();
	
	List<Message> messages = null;

	if (token == null) {
	    token = generateToken();
	}

	// The Magic is here
	String responseRoboMessage = robotService.getResponseToUser(user, newMessage);
	
	// Save request message
	Message requestMessage = messageService.saveMessage(newMessage, token, user, TypeMessage.REQUEST);

	// Save Robo message
	messageService.saveMessage(responseRoboMessage, token, user, TypeMessage.AUTO, requestMessage);
	
	// Whait for response backoffice chat
	try {
	    waitResponseManualMessage(user, requestMessage);
	    messages = messageService.findAndAddOldMessages(token, user);
	} catch (InterruptedException e) {
	   return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	ChatVO chatVO = MessageHelper.createVOMessage(messages);

	chatVO.setToken(token);

	return new ResponseEntity<ChatVO>(chatVO, HttpStatus.OK);
    }

    protected void addAllMenssage(List<Message> message, Message requestMessage, Message roboMessage) {
	message.add(requestMessage);
	message.add(roboMessage);
    }
    
    
    private Message waitResponseManualMessage(User user, Message requestMessage) throws InterruptedException {
	while (true) {
	    Message manualMessage = messageService.findManualResponseMessage(user, requestMessage, TypeMessage.MANUAL);

	    if (manualMessage != null) {
		return manualMessage;
	    }
	    Thread.sleep(3000l);
	}
    }

    private String generateToken() {
	return UUID.randomUUID().toString();
    }

}
