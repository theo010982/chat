package com.ciandt.chat.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ciandt.chat.helper.MessageHelper;
import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;
import com.ciandt.chat.service.MessageService;
import com.ciandt.chat.service.UserService;
import com.ciandt.chat.vo.ChatVO;
import com.ciandt.chat.vo.UserVO;

@Controller
@RequestMapping(value = "/protected/backoffice")
public class BackOfficeController extends BaseController {

    private static Integer FIRST_ELEMENT_TYPE_REQUEST = 1;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MessageService messageService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
	return new ModelAndView("backoffice_chat");
    }

    @RequestMapping(value = "/findUsers", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> findUsers() {

	List<User> users = userService.findAll();
	
	List<UserVO> usersVO = new ArrayList<UserVO>();

	for (User user : users) {
	    usersVO.add(new UserVO(user.getId(), user.getName()));
	}
	
	return new ResponseEntity<List<UserVO>>(usersVO, HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/findMessagebyUser", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> findMessagebyUser(@RequestHeader(required = true, value = "idUser") Integer idUser) {

	List<Message> messages = messageService.findLastMessagesByUser(idUser);

	if(messages.isEmpty()){
	    return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
	
	ChatVO chatVO = MessageHelper.createVOMessage(messages);

	return new ResponseEntity<ChatVO>(chatVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/saveResponseMessage", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> saveResponseMessage(@RequestHeader(required = true, value = "idUser") Integer idUser,
	    @RequestHeader(required = true, value = "responseMessage") String responseMessage,
	    @RequestHeader(required = true, value = "token") String token,
	    @RequestHeader(required = true, value = "lastMessageId") Long lastMessageId) {

	List<Message> messages = messageService.findLastMessagesByUser(idUser);

	User userToResponseMessage = userService.findById(idUser);

	/*
	 * Este recurso foi uma alternativa para pegar a ultima mensagem de request que o usuario do chat
	 * enviou, a posiçao 1 é pq a posicição é do tipo AUTO -(ROBO), e preciso do ID de referencia salvo.
	 */
	Message lastRequestMessage = messages.get(FIRST_ELEMENT_TYPE_REQUEST);
	
	Message manualMessage = messageService.saveMessage(responseMessage, token, userToResponseMessage,
		TypeMessage.MANUAL, lastRequestMessage);

	messages.add(manualMessage);

	ChatVO chatVO = MessageHelper.createVOMessage(messages);

	return new ResponseEntity<ChatVO>(chatVO, HttpStatus.OK);
    }

}
