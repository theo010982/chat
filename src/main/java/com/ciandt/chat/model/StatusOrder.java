package com.ciandt.chat.model;


public enum StatusOrder {
    PENDING, PROCESSING, WAITING_PROCESSING, COMPLETELY, CANCELED
}
