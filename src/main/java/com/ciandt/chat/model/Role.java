package com.ciandt.chat.model;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_CHAT
}
