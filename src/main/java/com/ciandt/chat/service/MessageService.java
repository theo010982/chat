package com.ciandt.chat.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;
import com.ciandt.chat.repository.MessageRestRepository;

@Service
public class MessageService {

    @Autowired
    private MessageRestRepository messageRestRepository;

    public Message saveMessage(Message message) {
	return messageRestRepository.save(message);
    }

    public List<Message> findMessageByTokenAndUser(String token, User user) {
	return messageRestRepository.findByUserAndToken(user, token);
    }

    public Message findManualResponseMessage(User user, Message messageRequest, TypeMessage typeMessage) {
	return messageRestRepository.findByUserAndRequestAndTypeMessage(user, messageRequest, typeMessage);
    }

    // Precisa melhorar, nao é possivel fazer Limit 1 no jpa normal, é
    // necessario usar criteria.
    public List<Message> findLastMessagesByUser(Integer idUser) {
	List<Message> messages = messageRestRepository.findLastTokenUsedByUser(idUser);

	Collections.sort(messages);

	List<Message> onlyLastMessages = new ArrayList<Message>();
	String token = null;

	if (messages.size() > 0) {
	    token = messages.get(0).getToken();
	}

	for (Message message : messages) {
	    if (message.getToken().equals(token)) {
		onlyLastMessages.add(message);
	    } else {
		break;
	    }
	}

	return onlyLastMessages;

    }

    public Message saveMessage(String newMessage, String token, User user, TypeMessage typeMessage) {
	Message message = saveMessage(token, newMessage, "KATRINA OPERADORA", user, typeMessage, null);
	return message;
    }

    public Message saveMessage(String newMessage, String token, User user, TypeMessage typeMessage,
	    Message requestMessage) {
	Message message = saveMessage(token, newMessage, "KATRINA OPERADORA", user, typeMessage, requestMessage);
	return message;
    }

    public List<Message> findAndAddOldMessages(String token, User user) {
	return findMessages(token, user);
    }

    private List<Message> findMessages(String token, User user) {
	return findMessageByTokenAndUser(token, user);
    }

    private Message saveMessage(String token, String newMessage, String operatorName, User user,
	    TypeMessage typeMessage, Message requestMessage) {
	Message message = new Message(new Date().getTime(), token, newMessage, operatorName, user, typeMessage,
		requestMessage);
	return saveMessage(message);
    }

    public Message findMessageById(Long lastMessageId) {
	return messageRestRepository.findOne(lastMessageId);
    }

}
