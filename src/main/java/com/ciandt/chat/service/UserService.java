package com.ciandt.chat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.chat.model.User;
import com.ciandt.chat.repository.UserRestRepository;

@Service
public class UserService {

    @Autowired
    private UserRestRepository userRepository;

    public User findByEmail(String email) {
	return userRepository.findByEmail(email);
    }

    public User findById(Integer userId) {
	return userRepository.findByID(userId);
    }

    public List<User> findAll() {
	return userRepository.findAll();
    }
}
