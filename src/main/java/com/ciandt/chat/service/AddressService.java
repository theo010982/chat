package com.ciandt.chat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.chat.model.Address;
import com.ciandt.chat.repository.AddressRestRepository;

@Service
public class AddressService {

    @Autowired
    AddressRestRepository addressRepository;

    public List<Address> findAddress() {
	return (List<Address>) addressRepository.findAllAddress();
    }

    public List<Address> findAddressByUser(Integer idUser) {
	return (List<Address>) addressRepository.findAddressByUser(idUser);
    }

}
