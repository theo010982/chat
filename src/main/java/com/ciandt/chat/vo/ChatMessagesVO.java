package com.ciandt.chat.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatMessagesVO implements Comparable<ChatMessagesVO>{
    
    private Long id;
    private String token;
    private Long seguencia;
    private String nomeOperador;
    private String mensagemTexto;
    private TypeMessage typeMessage;
    
    public ChatMessagesVO(Message message) {
	this.token = message.getToken();
	this.seguencia = message.getOrderMessage();
	this.mensagemTexto = message.getMessage();
	this.typeMessage = message.getTypeMessage();
	this.id = message.getId();
    }
    
    /**
     * @return the seguencia
     */
    public Long getSeguencia() {
        return seguencia;
    }
   
    /**
     * @return the nomeOperador
     */
    public String getNomeOperador() {
        return nomeOperador;
    }
    /**
     * @return the mensagemTexto
     */
    public String getMensagemTexto() {
        return mensagemTexto;
    }

    public TypeMessage getTypeMessage() {
        return typeMessage;
    }

    @Override
    public int compareTo(ChatMessagesVO o) {
	return this.seguencia.compareTo(o.seguencia);
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

   
    
}
