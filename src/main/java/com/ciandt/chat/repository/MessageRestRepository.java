package com.ciandt.chat.repository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;

@Service
public class MessageRestRepository {
    
    private static String FIND_MESSAGE_BY_USER_ID = "/message/findMessageByUserID/{idUser}/";
    private static String FIND_MESSAGE_BY_USER_AND_TOKEN = "/message/findByUserAndToken/{userID}/{token}";
    private static String FIND_MESSAGE_BY_USER_AND_REQUEST_MESSAGE_AND_TYPE = 
	    "/message/findByUserAndRequestAndTypeMessage/{userID}/{requestMessageID}/{typeMessage}";
    private static String FIND_MESSAGE_BY_LAST_MESSAGE_ID = "/message/findMessageByLastMessageID/{lastMessageId}/";
    private static String SAVE_MESSAGE = "/message/saveMessage";
    
    RestTemplate restTemplate = new RestTemplate();
    
    @Value("${hostlocation}")
    private String hostName;


    public List<Message> findLastTokenUsedByUser(Integer idUser) {
	Map<String, Integer> vars = new HashMap<String, Integer>();
	vars.put("idUser", idUser);
	
	ParameterizedTypeReference<List<Message>> typeRef = new ParameterizedTypeReference<List<Message>>() {	};
	ResponseEntity<List<Message>> response = restTemplate.exchange(hostName + FIND_MESSAGE_BY_USER_ID,
		HttpMethod.GET, null, typeRef, vars);
	
	return response.getBody();
    }
    
    
    public List<Message> findByUserAndToken(User user, String token){
	Map<String, Object> vars = new HashMap<String, Object>();
	vars.put("userID", user.getId());
	vars.put("token", token);
	
	ParameterizedTypeReference<List<Message>> typeRef = new ParameterizedTypeReference<List<Message>>() {	};
	ResponseEntity<List<Message>> response = restTemplate.exchange(hostName + FIND_MESSAGE_BY_USER_AND_TOKEN,
		HttpMethod.GET, null, typeRef, vars);
	
	return response.getBody();
    }

    public Message findByUserAndRequestAndTypeMessage(User user, Message request, TypeMessage typeMessage) {
	Map<String, Object> vars = new HashMap<String, Object>();
	vars.put("userID", user.getId());
	vars.put("requestMessageID", request.getId());
	vars.put("typeMessage", typeMessage);
	
	return restTemplate.getForObject(hostName + FIND_MESSAGE_BY_USER_AND_REQUEST_MESSAGE_AND_TYPE, Message.class, vars);
    }


    public Message findOne(Long lastMessageId) {
	Map<String, Object> vars = new HashMap<String, Object>();
	vars.put("lastMessageId", lastMessageId);
	return restTemplate.getForObject(hostName + FIND_MESSAGE_BY_LAST_MESSAGE_ID, Message.class, vars);
    }


    public Message save(Message message) {
	return restTemplate.postForObject(hostName + SAVE_MESSAGE, message, Message.class);
    }
    
}
