package com.ciandt.chat.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ciandt.chat.model.Address;

@Service
public class AddressRestRepository{
    
    private static String FIND_ALL_ADDRESS = "/address/findAllAddress/";
    private static String FIND_ADDRESS_BY_USER = "/address/findAddressByUser/{idUser}/";
    
    RestTemplate restTemplate = new RestTemplate();
    
    @Value("${hostlocation}")
    private String hostName;
    
    public List<Address> findAddressByUser(Integer idUser) {
	Map<String, Integer> vars = new HashMap<String, Integer>();
	vars.put("idUser", idUser);

	ParameterizedTypeReference<List<Address>> typeRef = new ParameterizedTypeReference<List<Address>>() {
	};
	ResponseEntity<List<Address>> response = restTemplate.exchange(hostName + FIND_ADDRESS_BY_USER, HttpMethod.GET,
		null, typeRef, vars);
	return response.getBody();
    }

    public List<Address> findAllAddress() {
	ParameterizedTypeReference<List<Address>> typeRef = new ParameterizedTypeReference<List<Address>>() {
	};
	ResponseEntity<List<Address>> response = restTemplate.exchange(hostName + FIND_ALL_ADDRESS, HttpMethod.GET,
		null, typeRef);
	return response.getBody();
    }
}
