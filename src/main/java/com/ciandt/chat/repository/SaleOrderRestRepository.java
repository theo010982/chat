package com.ciandt.chat.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ciandt.chat.model.SaleOrder;
import com.ciandt.chat.model.User;

@Service
public class SaleOrderRestRepository {
    
    private static String FIND_ORDER_BY_NUMBER = "/saleOrder/findOrderByNumber/{orderNumber}";
    private static String FIND_ORDER_BY_USER = "/saleOrder/findOrderByUser/{idUser}";
    
    RestTemplate restTemplate = new RestTemplate();
    
    @Value("${hostlocation}")
    private String hostName;
    
    public List<SaleOrder> findByUser(User user) {
	
	Map<String, Integer> vars = new HashMap<String, Integer>();
	vars.put("idUser", user.getId());

	ParameterizedTypeReference<List<SaleOrder>> typeRef = new ParameterizedTypeReference<List<SaleOrder>>() {
	};
	ResponseEntity<List<SaleOrder>> response = restTemplate.exchange(hostName + FIND_ORDER_BY_USER, HttpMethod.GET,
		null, typeRef, vars);
	return response.getBody();
    }

    public SaleOrder findOrderNumber(Integer orderNumber) {
	Map<String, Integer> vars = new HashMap<String, Integer>();
	vars.put("orderNumber", orderNumber);
	return restTemplate.getForObject(hostName + FIND_ORDER_BY_NUMBER, SaleOrder.class, vars);
    }

}
