package com.ciandt.chat.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ciandt.chat.model.User;

@Service
public class UserRestRepository {

    @Value("${hostlocation}")
    private String hostName;

    private static String FIND_USER_BY_EMAIL = "/user/findUserByEmail/{email}/";
    private static String FIND_USER_BY_ID = "/user/findUserByID/{id}/";
    private static String FIND_ALL = "/user/findAll";

    RestTemplate restTemplate = new RestTemplate();

    public User findByEmail(String email) {
	Map<String, String> vars = new HashMap<String, String>();
	vars.put("email", email);
	return restTemplate.getForObject(hostName + FIND_USER_BY_EMAIL, User.class, vars);

    }

    public User findByID(Integer id) {
	Map<String, Integer> vars = new HashMap<String, Integer>();
	vars.put("id", id);
	return restTemplate.getForObject(hostName + FIND_USER_BY_ID, User.class, vars);

    }

    public List<User> findAll() {
	ParameterizedTypeReference<List<User>> typeRef = new ParameterizedTypeReference<List<User>>() {	};
	ResponseEntity<List<User>> response = restTemplate.exchange(hostName + FIND_ALL, HttpMethod.GET, null, typeRef);
	return response.getBody();

    }

}
