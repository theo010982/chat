package com.ciandt.chat.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.ciandt.chat.model.User;
import com.ciandt.chat.repository.UserRestRepository;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRestRepository userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	
	User user  = null;
	String username = authentication.getName();
	String password = (String) authentication.getCredentials();

	if(username != null && username != "") {
	    
	    user = userService.findByEmail(username);
	}

	if (user == null || !user.getPassword().equals(password)) {
	    throw new BadCredentialsException("Username or password not found.");
	}

	return new UsernamePasswordAuthenticationToken(username, password, null);
    }

    @Override
    public boolean supports(Class<?> arg0) {
	return true;
    }
}
