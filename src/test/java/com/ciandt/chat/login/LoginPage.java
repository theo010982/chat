package com.ciandt.chat.login;

import com.ciandt.chat.seleniumCore.SeleliumBase;


public class LoginPage extends SeleliumBase{
	
	private static final String LOGIN_PAGE = "localhost:9999/";

	public void openLoginURL() {
		super.openFireFoxDriver();
		openUrl(LOGIN_PAGE);
	}

	public void loggerUser() {
		super.fillElementById("j_username", "theo@gmail.com");
		super.fillElementById("j_password", "123");
		super.clickElementById("btnLogin");
	}
	
	 
	
}
