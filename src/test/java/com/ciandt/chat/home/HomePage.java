package com.ciandt.chat.home;

import org.junit.Assert;

import com.ciandt.chat.seleniumCore.SeleliumBase;

public class HomePage extends SeleliumBase{
	
	public void verifyIsLogged(){
		String welcome = super.getTextElementByClassName("muted");
		Assert.assertEquals("Bem Vindo a carteira de vacinação.", welcome);
	}
	
	public void getContactsPage(){
		super.clickElementById("navContacts");
	}
	
	public void verifyName(){
		String x = super.getTextElementByClassName("lead");
		Assert.assertEquals("Carteirinha de vacinação onLine - Seu pet merece o melhor.", x);
	}
}
